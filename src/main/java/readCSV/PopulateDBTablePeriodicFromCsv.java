package readCSV;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class PopulateDBTablePeriodicFromCsv {

    public static void main(String[] args) throws Exception {

        Connection connection = ConstantsUtil.getConnection();
        Long beginTime = ConstantsUtil.curTime;
        TableToTableSync t = new TableToTableSync();
        Thread thread = new Thread(t);
        thread.start();
        readCsvFileWritetoTable( connection, beginTime);

    }

    public static void readCsvFileWritetoTable( Connection connection, Long beginTime) throws Exception {
        String csvFilePath = "src/main/resources/sample_PII_src.csv";
        int batchSize = 2;
        try {
            BufferedReader lineReader = new BufferedReader(new FileReader(csvFilePath));
            String lineText = null;

            String sql1 = "Truncate table test_db.pii_src;";
            PreparedStatement statement1 = connection.prepareStatement(sql1);
            statement1.execute();

            String sql = "INSERT INTO test_db.pii_src\n" +
                    "(accountzip, accountphonenumber, issued, firstdiagnosisdate, currsntdiagnosisdate, dob)\n" +
                    "VALUES(?, ?, ?, ?, ?, ?);";
            PreparedStatement statement = connection.prepareStatement(sql);
            int count = 2;

            lineReader.readLine(); // skip header line
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            createStatement(beginTime, lineReader, statement, sdf);

            lineReader.close();
            connection.close();

        } catch (IOException ex) {
            System.err.println(ex);
        } catch (SQLException ex) {
            ex.printStackTrace();

            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private static void createStatement(Long beginTime, BufferedReader lineReader, PreparedStatement statement, DateFormat sdf) throws IOException, InterruptedException, SQLException {
        String lineText;
        while ((lineText = lineReader.readLine()) != null){
            System.out.println("Start..." + sdf.format(new Date()));

            TimeUnit.MILLISECONDS.sleep(ConstantsUtil.insertDelay);

            String[] data = lineText.split(",");
            System.out.println(data.toString());
            Long accountzip = Long.parseLong(data[0]);
            String accountphonenumber = data[1];
            Long issued = beginTime;

            String firstdiagnosisdate = data[3];
            String currsntdiagnosisdate =  data[4];
            String dob =  data[5];

            statement.setLong(1, accountzip);
            statement.setString(2, accountphonenumber);

            statement.setLong(3, issued);

            statement.setString(4, firstdiagnosisdate);

            statement.setString(5, currsntdiagnosisdate);
            statement.setString(6, dob);

            statement.execute();
            System.out.println("issued...: " + issued);
            beginTime = beginTime + ConstantsUtil.insertDelay;
        }
    }




}