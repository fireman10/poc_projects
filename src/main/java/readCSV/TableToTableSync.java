package readCSV;

import java.sql.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;


public class TableToTableSync implements Runnable {

    public void run() {
        Connection connection = null;
        try {
            Thread.sleep(ConstantsUtil.SyncDelay*3);
            connection = ConstantsUtil.getConnection();
            readFromTableWritetoTable(connection, ConstantsUtil.curTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {

        Connection connection = ConstantsUtil.getConnection();

        String sql1 = "Truncate table test_db.pii_target;";
        PreparedStatement statement1 = connection.prepareStatement(sql1);
        statement1.execute();
        Long beginTime = ConstantsUtil.curTime;
        System.out.println("Start...SQL" + new Date());
        // delay 70 seconds
        TimeUnit.SECONDS.sleep(5);
        readFromTableWritetoTable(connection, beginTime);
        System.out.println("End...SQL" + new Date());

    }

    public static void readFromTableWritetoTable(Connection connection, Long beginTime) throws Exception {


        String sql2 = "Truncate table test_db.pii_target;";
        PreparedStatement statement1 = connection.prepareStatement(sql2);
        statement1.execute();
        int count = 1;
        Long from = beginTime;
        Long to = beginTime;
        String sql = "INSERT INTO test_db.pii_target\n" +
                "(accountzip, accountphonenumber, issued, firstdiagnosisdate, currsntdiagnosisdate, dob)\n" +
                "VALUES(?, ?, ?, ?, ?, ?);";
        while (true) {
            from = to;
            to = from + ConstantsUtil.SyncDelay;

            try {
                String sql1 = "select accountzip, accountphonenumber, issued, firstdiagnosisdate, currsntdiagnosisdate, dob from test_db.pii_src where  issued >= " + from + " and issued < " + to + ";";
                Statement statement2 = connection.createStatement();
                ResultSet rs = statement2.executeQuery(sql1);
                System.out.println(sql1);

                PreparedStatement statement = connection.prepareStatement(sql);
                insertData(rs, statement, count);
                count ++;
                // delay 60 seconds
                TimeUnit.MILLISECONDS.sleep(ConstantsUtil.SyncDelay);

            } catch (SQLException ex) {
                System.err.println(ex);
                ex.printStackTrace();
                try {
                    connection.close();
                    connection.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private static void insertData(ResultSet rs, PreparedStatement statement, int count) throws SQLException {
        while (rs.next()) {


            Long accountzip = rs.getLong("accountzip");
            String accountphonenumber = rs.getString("accountphonenumber");
            Long issued = rs.getLong("issued");
            String firstdiagnosisdate = rs.getString("firstdiagnosisdate");
            String currsntdiagnosisdate = rs.getString("currsntdiagnosisdate");
            String dob = rs.getString("dob");

            statement.setLong(1, accountzip);
            statement.setString(2, accountphonenumber);
            statement.setLong(3, issued);
            statement.setString(4, firstdiagnosisdate);
            statement.setString(5, currsntdiagnosisdate);
            statement.setString(6, dob);
            System.out.println("inserted - ");
            if(count%50 !=0) {
                System.out.println("Count --- "+count);
                statement.execute();
            }
            //to = from;
        }

    }


}