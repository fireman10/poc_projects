package readCSV;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConstantsUtil {
    public final static Long curTime = System.currentTimeMillis();
    public final static int insertDelay = 2000;  //milliseconds
    public final static int SyncDelay = 3*insertDelay; // milliseconds


    public static Connection getConnection() throws Exception {

        String jdbcURL = "jdbc:mysql://54.86.47.129:3306/test_db?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String username = "demouser1";
        String password = "demopassword";

        Connection connection = null;

        try {
            Driver d = (Driver) Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            DriverManager.registerDriver(d);
            // Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection(jdbcURL, username, password);
            // connection.setAutoCommit(false);
        }  catch (SQLException ex) {
            ex.printStackTrace();

            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }
}
